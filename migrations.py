from config import db, cursor

print('Membuat tabel users ...')

sql = '''create table if not exists users (
        id int unsigned auto_increment primary key,
        username varchar(100) unique not null,
        email varchar(255) unique not null,
        fullname varchar(255) not null,
        fotoprofil varchar(255) default '',
        tgllahir date,
        gender char(2) default 'lk',
        password varchar(255)
    ) engine=INNODB
    '''

result = cursor.execute(sql)

cursor.close()
db.close()

print('migrasi selesai')