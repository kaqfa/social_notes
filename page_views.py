from config import app, cursor, bcrypt
from flask import render_template, request, session, redirect, url_for
from datetime import datetime
from werkzeug.utils import secure_filename

@app.route('/')
def index():
    if 'user' in session:
        return render_template('catatanku.html')
    else:
        return render_template('index.html')

@app.route('/catatanku')
def catatanku():
    if 'user' in session:
        return render_template('catatanku.html')
    else:
        return redirect(url_for('login'))

@app.route('/members')
def members():
    if 'user' in session:
        sql = 'select * from users'
        cursor.execute(sql)
        dataMember = cursor.fetchall()
        return render_template('members.html', members=dataMember)
    else:
        return redirect(url_for('login'))

@app.route('/profil')
def profil():
    if 'user' in session:
        profil = {'fullname': 'Fahri Firdausillah'}
        return render_template('profil.html', profil=profil)
    else:
        return redirect(url_for('login'))

@app.route('/form-profil')
def form_profil():
    user = None
    if 'user' in session :
        sql = 'select * from users where username = %s'
        cursor.execute(sql, [session['user']['username']])
        user = cursor.fetchone()
        return render_template('form_profil.html', user=user)
    else:
        return redirect(url_for('login'))
    

@app.route('/form-profil', methods=["POST"])
def post_profil():
    sql = 'update users set fullname = %(fullname)s, tgllahir = %(tgllahir)s, gender = %(gender)s'
    data = request.form.to_dict()
    profpic = request.files['fotoprofil']
    if profpic:
        data['fotoprofil'] = f'pp_{data["username"]}_{secure_filename(profpic.filename)}'
        profpic.save(f'./static/images/upload/{data["fotoprofil"]}')
        sql += ' , fotoprofil = %(fotoprofil)s '

    if data['password'] and data['password'] == request.form['confirm_password']:
        sql += ' , password = %(password)s '
        data['password'] = bcrypt.generate_password_hash(data['password']).decode('utf-8')
    sql += ' where id = %(id)s'

    res = cursor.execute(sql, data)

    return redirect(url_for('form_profil'))