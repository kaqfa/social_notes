from config import app, bcrypt, db, cursor
from flask import render_template, request
from flask import redirect, url_for, flash, session
from werkzeug.utils import secure_filename

@app.route('/login')
def login():
    return render_template('login.html.jinja')

@app.route('/login', methods=['POST'])
def post_login():
    username = request.form["username"]
    password = request.form["password"]

    cursor.execute('select * from users where username = %s', (username,))
    user = cursor.fetchone()

    if user :
        hashed_pass = user['password']
        if bcrypt.check_password_hash(hashed_pass, password):
            session['user'] = user
            return redirect(url_for('catatanku'))
    
    return render_template('login.html.jinja', error='Kombinasi user dan password tidak ditemukan')

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/register', methods=['POST'])
def post_register():
    sql = '''insert into users values
        (null, %(username)s, %(email)s, %(fullname)s, %(fotoprofil)s, %(tgllahir)s, %(gender)s, %(password)s)
    '''

    password = request.form['password']
    if password != request.form['confirm_password']:
        return render_template('register.html', error='username dan password harus sama', formdata=data)
    
    hashed_pass = bcrypt.generate_password_hash(password).decode('utf-8')
    
    data = {
            'username': request.form['username'],
            'email': request.form['email'],
            'fullname': request.form['fullname'],
            'tgllahir': request.form['tgllahir'],
            'fotoprofil': '',
            'gender': request.form['gender'],
            'password': hashed_pass
        }
    
    profpic = request.files['fotoprofil']
    if profpic:
        data['fotoprofil'] = f'pp_{data["username"]}_{secure_filename(profpic.filename)}'
        profpic.save(f'./static/images/upload/{data["fotoprofil"]}')
    
    cursor.execute(sql, data)
    
    flash({'register': 'Registrasi berhasil'})
    return redirect(url_for('login'))

@app.route('/logout')
def logout():
    session.pop('user')
    return redirect(url_for('index'))