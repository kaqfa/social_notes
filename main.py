from datetime import datetime
from config import app

@app.context_processor
def inject_date():
    return dict(tgl=datetime.utcnow())

from auth_views import *
from page_views import *

if __name__ == '__main__':
    app.secret_key = b'rahasiakitabersamainiseharusnyateksyangsangatpanjang'
    app.run(host='0.0.0.0', port=55, debug=True)