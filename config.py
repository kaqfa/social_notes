from flask import Flask
from flask_bcrypt import Bcrypt

import pymysql

app = Flask(__name__)
bcrypt = Bcrypt(app)

db_user = 'root'
db_pass = 'tiger'
db_host = 'localhost'
db_name = 'socialnotes'

db = pymysql.connect(host=db_host, user=db_user, password=db_pass, db=db_name)
charset = 'utf8mb4'

cursor = db.cursor(pymysql.cursors.DictCursor)